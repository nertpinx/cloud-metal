mod schemas;
mod state;
use kube_client::ResourceExt;
use schemas::*;
use state::State;

use futures_util::StreamExt;
use kube::{
    runtime::{controller::Action, finalizer, watcher, Controller},
    Api, Client,
};
use std::{sync::Arc, time::Duration};
use tracing::*;
use tracing_subscriber::prelude::*;
use tracing_subscriber::{EnvFilter, Registry};

#[derive(thiserror::Error, Debug)]
pub(crate) enum Error {
    #[error("Kube error: {0}")]
    KubeError(#[from] kube::Error),

    #[error("Finalizer error: {0}")]
    FinalizerError(String),

    // #[error("Error locking mutex: {0}")]
    // MutexError(String),

    #[error("Missing namespace of object")]
    MissingNamespace,

    #[error("Error parsing log settings: {0}")]
    ParseError(#[from] tracing_subscriber::filter::ParseError),

    #[error("Error setting global parsing log settings: {0}")]
    TracingError(#[from] tracing::subscriber::SetGlobalDefaultError),
}

pub(crate) type Result<T> = std::result::Result<T, Error>;

impl<E: std::error::Error> From<finalizer::Error<E>> for Error {
    fn from(e: finalizer::Error<E>) -> Self {
        Self::FinalizerError(e.to_string())
    }
}

// impl<E> From<std::sync::PoisonError<E>> for Error {
//     fn from(e: std::sync::PoisonError<E>) -> Self {
//         Self::MutexError(e.to_string())
//     }
// }

#[instrument(level = "info", skip(object, ctx), fields(vmi_name))]
async fn reconcile(object: Arc<VirtualMachineInstance>, ctx: Arc<State>) -> Result<Action> {
    tracing::Span::current().record("vmi_name", &object.name_any());

    let ns = object.namespace().ok_or(Error::MissingNamespace)?;
    let api: Api<VirtualMachineInstance> = Api::namespaced(ctx.get_client(), &ns);

    Ok(finalizer(&api, "cloud-metal.k8r.cz/created", object,
                 |ev| async move { ctx.reconcile(ev).await }).await?)
}

fn error_policy(_object: Arc<VirtualMachineInstance>, _error: &Error, _ctx: Arc<State>) -> Action {
    Action::requeue(Duration::from_secs(10))
}

fn setup_logging() -> Result<()> {
    let env_filter = EnvFilter::try_from_default_env()
        .or_else(|_| EnvFilter::try_new("warn,cloud_metal=info"))?;

    if is_terminal::is_terminal(std::io::stdout()) {
        let logger = tracing_subscriber::fmt::layer().compact();
        let subscriber = Registry::default().with(logger).with(env_filter);
        tracing::subscriber::set_global_default(subscriber)?;
    } else {
        let logger = tracing_subscriber::fmt::layer().json();
        let subscriber = Registry::default().with(logger).with(env_filter);
        tracing::subscriber::set_global_default(subscriber)?;
    }

    info!("Logging set up");

    Ok(())
}

#[tokio::main]
async fn main() -> Result<()> {
    setup_logging()?;

    let client = Client::try_default().await?;
    let context = Arc::new(State::new(client.clone()));
    let api: Api<VirtualMachineInstance> = Api::all(client);

    let config = watcher::Config::default().labels("kubevirt.io/nodeName=krustlet");

    info!("Starting the controller");

    Controller::new(api.clone(), config.clone())
        .shutdown_on_signal()
        .run(reconcile, error_policy, context)
        .for_each(|res| async move {
            match res {
                Ok((o, _)) => debug!(vmi = ?o.name, "reconciled"),
                Err(e) => warn!(error = ?e, "reconciliation failed"),
            }
        })
        .await;

    Ok(())
}
