#[derive(Debug, serde::Serialize, serde::Deserialize, Default, Clone, schemars::JsonSchema)]
pub struct Requests {
    pub memory: String,
}

#[derive(Debug, serde::Serialize, serde::Deserialize, Default, Clone, schemars::JsonSchema)]
pub struct Resources {
    pub requests: Requests,
}

#[derive(Debug, serde::Serialize, serde::Deserialize, Default, Clone, schemars::JsonSchema)]
pub struct Domain {
    pub resources: Resources,
}

#[derive(
    kube::CustomResource,
    Debug,
    serde::Serialize,
    serde::Deserialize,
    Default,
    Clone,
    schemars::JsonSchema,
)]
#[kube(
    group = "kubevirt.io",
    version = "v1",
    kind = "VirtualMachineInstance",
    namespaced
)]
pub struct VirtualMachineInstanceSpec {
    pub domain: Domain,
}
