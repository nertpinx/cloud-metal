use std::{collections::BTreeMap, sync::Arc};

use crate::schemas::*;
use crate::{Error, Result};
use futures_util::lock::Mutex;
use kube::runtime::{controller::Action, finalizer};
use kube_client::{Client, ResourceExt};
use tracing::*;

fn get_key(object: &VirtualMachineInstance) -> String {
    let mut key = String::new();

    if let Some(ns) = object.namespace() {
        key += &ns;
        key += "/";
    }
    key += &object.name_any();

    key
}

pub(crate) struct State {
    client: Client,

    vmis: Mutex<BTreeMap<String, VirtualMachineInstanceSpec>>,
}

fn update_vmi(old_spec: &mut VirtualMachineInstanceSpec, new_spec: &VirtualMachineInstanceSpec) {
    info!(old = ?old_spec, new = ?new_spec, "updating");
    old_spec.domain.resources.requests.memory = new_spec.domain.resources.requests.memory.clone();
}

fn create_vmi(spec: VirtualMachineInstanceSpec) -> VirtualMachineInstanceSpec {
    info!("New object");
    spec
}

impl State {
    pub fn new(client: Client) -> Self {
        Self {
            client,
            vmis: Mutex::new(Default::default()),
        }
    }

    pub fn get_client(&self) -> Client {
        self.client.clone()
    }

    pub async fn reconcile(
        &self,
        event: finalizer::Event<VirtualMachineInstance>,
    ) -> Result<Action> {
        match event {
            finalizer::Event::Apply(o) => self.apply(&o).await,
            finalizer::Event::Cleanup(o) => self.cleanup(&o).await,
        }
    }

    pub async fn apply(&self, object: &Arc<VirtualMachineInstance>) -> Result<Action> {
        let key = get_key(object);
        let new_spec = object.spec.clone();

        let mut vmis = self.vmis.lock().await;

        vmis.entry(key)
            .and_modify(|v| update_vmi(v, &new_spec))
            .or_insert_with(|| create_vmi(object.spec.clone()));

        Ok::<Action, Error>(Action::await_change())
    }

    pub async fn cleanup(&self, object: &Arc<VirtualMachineInstance>) -> Result<Action> {
        tracing::Span::current().record("vmi_name", &object.name_any());

        let key = get_key(object);

        let mut vmis = self.vmis.lock().await;
        if vmis.contains_key(&key) {
            info!("Removing");
            vmis.remove(&key);
        } else {
            info!("Cleaning up from previous run");
        }

        Ok::<Action, Error>(Action::await_change())
    }
}
